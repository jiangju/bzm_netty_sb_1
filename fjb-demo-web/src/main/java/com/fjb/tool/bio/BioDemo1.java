package com.fjb.tool.bio;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.xml.sax.InputSource;

public class BioDemo1 {
	
	
	public static void printState(Socket socket) {

		System.out.println("isInputShutdown:" + socket.isInputShutdown());
	
		System.out.println("isOutputShutdown:" + socket.isOutputShutdown());
	
		System.out.println("isClosed:" + socket.isClosed());
	
		System.out.println();

	}

	 

	public static void main(String[] args) throws Exception {

		Socket socket = new Socket("www.ptpress.com.cn", 80);
		printState(socket);
	
		socket.shutdownInput();
		printState(socket);
	
		socket.shutdownOutput();
		printState(socket);

	}

	
}
