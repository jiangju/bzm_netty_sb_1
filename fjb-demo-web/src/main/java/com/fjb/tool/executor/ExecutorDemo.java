package com.fjb.tool.executor;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description:线程池demo
 * @author hemiao
 * @time:2020年4月7日 下午8:12:58
 */
public class ExecutorDemo {

	//Executors
		
	public static void main(String[] args) {
		
		
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
		ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
		ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(3);
		ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
		ExecutorService newWorkStealingPool = Executors.newWorkStealingPool();
		
		
		
		//ExecutorService	
		
		LinkedList<Object> linkedList = new LinkedList<Object>();
		
	}
	
}
