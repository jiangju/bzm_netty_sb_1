package com.fjb.pojo.im;

import java.time.LocalDateTime;

/**
 * @Description:聊天消息内容
 * @author hemiao
 * @time:2020年5月5日 下午9:37:27
 */
public class ImMsgContent {
	
	// "from": "来源ID",
	private Integer fromId;
	
	// "to": "目标ID",
	private Integer toId;
	
	// "cmd":"命令码(11)int类型",
	private Integer cmd;
	
	// "createTime": "消息创建时间long类型",
	private LocalDateTime createTime;
	
	// "msgType": "消息类型int类型(0:text、1:image、2:voice、3:vedio、4:music、5:news)",
	private Integer msgType;
	
	// "chatType":"聊天类型int类型(0:未知,1:公聊,2:私聊)",
	private Integer chatType;
	
	// "groupId":"群组id仅在chatType为(1)时需要,String类型",
	private Integer groupId;
	
	// "content": "内容",
	private String content;
	
	// "extras" : "扩展字段,JSON对象格式如：{'扩展字段名称':'扩展字段value'}"
	private String extras;

	public Integer getFromId() {
		return fromId;
	}

	public void setFromId(Integer fromId) {
		this.fromId = fromId;
	}

	public Integer getToId() {
		return toId;
	}

	public void setToId(Integer toId) {
		this.toId = toId;
	}

	public Integer getCmd() {
		return cmd;
	}

	public void setCmd(Integer cmd) {
		this.cmd = cmd;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public Integer getMsgType() {
		return msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}

	public Integer getChatType() {
		return chatType;
	}

	public void setChatType(Integer chatType) {
		this.chatType = chatType;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getExtras() {
		return extras;
	}

	public void setExtras(String extras) {
		this.extras = extras;
	}
	
}
