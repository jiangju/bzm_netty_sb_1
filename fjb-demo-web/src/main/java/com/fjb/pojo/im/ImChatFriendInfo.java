package com.fjb.pojo.im;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * im_chat 朋友信息表
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public class ImChatFriendInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer mainUserId;

    private Integer userId;

    /**
     * 朋友组id  
     */
    private Integer friendGroupId;

    /**
     * 朋友用户id
     */
    private Integer friendUserId;

    /**
     * 朋友备注
     */
    private String friendRemark;

    private LocalDateTime createTime;

    private Integer createUserId;

    private LocalDateTime updateTime;

    private Integer updateUserId;

    /**
     * 好友备注
     */
    private String remark;

    /**
     * 数据状态   1、正常     2、删除
     */
    private Integer dataStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMainUserId() {
        return mainUserId;
    }

    public void setMainUserId(Integer mainUserId) {
        this.mainUserId = mainUserId;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Integer getFriendGroupId() {
        return friendGroupId;
    }

    public void setFriendGroupId(Integer friendGroupId) {
        this.friendGroupId = friendGroupId;
    }
    public Integer getFriendUserId() {
        return friendUserId;
    }

    public void setFriendUserId(Integer friendUserId) {
        this.friendUserId = friendUserId;
    }
    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public Integer getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Integer updateUserId) {
        this.updateUserId = updateUserId;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Integer dataStatus) {
        this.dataStatus = dataStatus;
    }

    @Override
    public String toString() {
        return "ImChatFriendInfo{" +
        "id=" + id +
        ", mainUserId=" + mainUserId +
        ", userId=" + userId +
        ", friendGroupId=" + friendGroupId +
        ", friendUserId=" + friendUserId +
        ", friendRemark=" + friendRemark +
        ", createTime=" + createTime +
        ", createUserId=" + createUserId +
        ", updateTime=" + updateTime +
        ", updateUserId=" + updateUserId +
        ", remark=" + remark +
        ", dataStatus=" + dataStatus +
        "}";
    }
}
