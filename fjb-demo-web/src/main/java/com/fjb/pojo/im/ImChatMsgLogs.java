package com.fjb.pojo.im;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * im_chat聊天记录
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public class ImChatMsgLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 日志类型   1、系统消息
     */
    private Integer logsType;

    /**
     * 主账号id
     */
    private Integer mainUserId;

    /**
     * 用户id  消息属于谁的
     */
    private Integer userId;

    /**
     * 朋友信息表id
     */
    private Integer friendInfoId;

    /**
     * 群信息id  当消息类型为 2时
     */
    private Integer groupInfoId;

    /**
     * 发送类型：1、点对点个人消息  2、群消息
     */
    private Integer toType;

    /**
     * 发送者id
     */
    private String sendId;

    /**
     * 发送时间
     */
    private LocalDateTime sendTime;

    /**
     * 接受者id
     */
    private String receiveId;

    /**
     * 读取时间
     */
    private LocalDateTime receiveTime;

    /**
     * 消息类型   1 表示文本消息, 2 表示图片， 3 表示语音， 4 表示视频，
     */
    private String msgType;

    /**
     * 消息内容
     */
    private String msgContent;

    /**
     * 消息读状态    1、已读    ，  2、未读
     */
    private Integer msgReadStatus;

    /**
     * 离线消息    1、在线  ， 2、离线
     */
    private Integer msgOfflineStatus;

    private LocalDateTime createTime;

    private String createUserId;

    /**
     * 聊天信息版本 1.0.0 开始
     */
    private String chatVersion;

    /**
     * 数据来源：  1000、电脑网页 （默认）
     */
    private Integer dataSources;

    /**
     * 发送者ip地址
     */
    private String ipSendLocation;

    /**
     * 接受者ip地址
     */
    private String ipReceiveLocation;

    /**
     * 是否是好友关系  1、好友   2、不是
     */
    private Integer checkFriend;

    /**
     * 数据状态   1、正常     2、删除
     */
    private Integer dataStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getLogsType() {
        return logsType;
    }

    public void setLogsType(Integer logsType) {
        this.logsType = logsType;
    }
    public Integer getMainUserId() {
        return mainUserId;
    }

    public void setMainUserId(Integer mainUserId) {
        this.mainUserId = mainUserId;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Integer getFriendInfoId() {
        return friendInfoId;
    }

    public void setFriendInfoId(Integer friendInfoId) {
        this.friendInfoId = friendInfoId;
    }
    public Integer getGroupInfoId() {
        return groupInfoId;
    }

    public void setGroupInfoId(Integer groupInfoId) {
        this.groupInfoId = groupInfoId;
    }
    public Integer getToType() {
        return toType;
    }

    public void setToType(Integer toType) {
        this.toType = toType;
    }
    public String getSendId() {
        return sendId;
    }

    public void setSendId(String sendId) {
        this.sendId = sendId;
    }
    public LocalDateTime getSendTime() {
        return sendTime;
    }

    public void setSendTime(LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }
    public String getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(String receiveId) {
        this.receiveId = receiveId;
    }
    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }
    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }
    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }
    public Integer getMsgReadStatus() {
        return msgReadStatus;
    }

    public void setMsgReadStatus(Integer msgReadStatus) {
        this.msgReadStatus = msgReadStatus;
    }
    public Integer getMsgOfflineStatus() {
        return msgOfflineStatus;
    }

    public void setMsgOfflineStatus(Integer msgOfflineStatus) {
        this.msgOfflineStatus = msgOfflineStatus;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }
    public String getChatVersion() {
        return chatVersion;
    }

    public void setChatVersion(String chatVersion) {
        this.chatVersion = chatVersion;
    }
    public Integer getDataSources() {
        return dataSources;
    }

    public void setDataSources(Integer dataSources) {
        this.dataSources = dataSources;
    }
    public String getIpSendLocation() {
        return ipSendLocation;
    }

    public void setIpSendLocation(String ipSendLocation) {
        this.ipSendLocation = ipSendLocation;
    }
    public String getIpReceiveLocation() {
        return ipReceiveLocation;
    }

    public void setIpReceiveLocation(String ipReceiveLocation) {
        this.ipReceiveLocation = ipReceiveLocation;
    }
    public Integer getCheckFriend() {
        return checkFriend;
    }

    public void setCheckFriend(Integer checkFriend) {
        this.checkFriend = checkFriend;
    }
    public Integer getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Integer dataStatus) {
        this.dataStatus = dataStatus;
    }

    @Override
    public String toString() {
        return "ImChatMsgLogs{" +
        "id=" + id +
        ", logsType=" + logsType +
        ", mainUserId=" + mainUserId +
        ", userId=" + userId +
        ", friendInfoId=" + friendInfoId +
        ", groupInfoId=" + groupInfoId +
        ", toType=" + toType +
        ", sendId=" + sendId +
        ", sendTime=" + sendTime +
        ", receiveId=" + receiveId +
        ", receiveTime=" + receiveTime +
        ", msgType=" + msgType +
        ", msgContent=" + msgContent +
        ", msgReadStatus=" + msgReadStatus +
        ", msgOfflineStatus=" + msgOfflineStatus +
        ", createTime=" + createTime +
        ", createUserId=" + createUserId +
        ", chatVersion=" + chatVersion +
        ", dataSources=" + dataSources +
        ", ipSendLocation=" + ipSendLocation +
        ", ipReceiveLocation=" + ipReceiveLocation +
        ", checkFriend=" + checkFriend +
        ", dataStatus=" + dataStatus +
        "}";
    }
}
