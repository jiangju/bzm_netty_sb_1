package com.fjb.controller.index;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
	
	@RequestMapping(value = "/")
    public String showDefault(HttpServletRequest request){
    			
		//return "/index";
		return "/im/im_chat_info";
    }
	
	@RequestMapping(value = "/console")
    public ModelAndView showConsole(HttpServletRequest request){
		ModelAndView mv = new ModelAndView();
		// 项目  		project
		mv.addObject("project", "爱技术、爱生活、啦啦啦啦啦啦");
    	// 版本号		version
		mv.addObject("version", "1.0.0");
		// 框架  		framework
		mv.addObject("framework", "springboot、mybatis、mysql、layui");
		// 特色  		feature 
		mv.addObject("feature", "java全家桶 、啦啦啦啦啦啦");
		// 作者        		author
		String author = "今晚打老虎";
		mv.addObject("author", author);
		// 联系方式 	contact
		mv.addObject("contact", "邮箱 hm68001531@163.com , QQ 68001531 , QQ交流群  695449401");
		mv.setViewName("/console");
		return mv;
    }
	
}
