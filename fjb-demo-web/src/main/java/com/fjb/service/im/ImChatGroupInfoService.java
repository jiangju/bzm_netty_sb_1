package com.fjb.service.im;

import com.fjb.pojo.im.ImChatGroupInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * im_chat聊天组信息 服务类
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatGroupInfoService extends IService<ImChatGroupInfo> {

}
