package com.fjb.service.im;

import com.fjb.pojo.im.ImChatMsgLogs;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * im_chat聊天记录 服务类
 * </p>
 *
 * @author hemiao
 * @since 2020-05-26
 */
public interface ImChatMsgLogsService extends IService<ImChatMsgLogs> {
	
	/**
	 * @Description:添加消息
	 * @param logs
	 * @return
	 * Integer
	 * @exception:
	 * @author: hemiao
	 * @time:2020年5月27日 上午9:52:04
	 */
	Integer saveWebMsgLogs(ImChatMsgLogs logs);
	
	/**
	 * @Description:修改休息为离线
	 * @param msgId
	 * @return
	 * Integer
	 * @exception:
	 * @author: hemiao
	 * @time:2020年5月31日 下午9:31:42
	 */
	Integer updateOfflineStatusTwo(Integer msgId);
	
	/**
	 * @Description:消息读取
	 * @param msgIdList
	 * void
	 * @exception:
	 * @author: hemiao
	 * @time:2020年5月31日 下午9:41:22
	 */
	void updateMsgReadStatusOne(List<String> msgIdList);
}
